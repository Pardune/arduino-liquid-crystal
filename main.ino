/*
A multifunctional device based on Arduino. Connected is a LCD-screen and
several sensors.

Liquid Crystal is connected to an Arduino. Several sensors and a button are attached, too.
LC-Display shows the current state of the running program.

The circuit:
* LCD RS pin to digital pin 12
* LCD Enable pin to digital pin 11
* LCD D4 pin to digital pin 5
* LCD D5 pin to digital pin 4
* LCD D6 pin to digital pin 3
* LCD D7 pin to digital pin 2
* LCD R/W pin to ground
* 10K resistor:
* ends to +5V and ground
* wiper to LCD VO pin (pin 3)

The LiquidCrystal library works with all LCD displays that are compatible with Hitachi HD44780

By Pardune 2016

*/

// imports

#include <LiquidCrystal.h>

// declare pins according to wiring plan.
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);



// setup
void setup() {
  bool changedProg = false;

  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  
  // Pins
  
  // Analog input pin that connects to a potentiometer
  const int analogInPin = A0;
  
  // serial comm. To debug and for errors.
  Serial.begin(9600);
}


// Game. while button is pressed, print millis.
// Try to make it round numbers!
// Can also be used as a stopwatch.
void stopGame() {

  
  bool upd = true;
  bool state = true;
  countChanged = false;
  changedProg = false;

  while (true) {
    pinMode(9, OUTPUT);
    digitalWrite(9, HIGH);
    delay(10);
    pinMode(9, INPUT);

    digitalWrite(9, HIGH);
    digitalWrite(10, HIGH);
    delay(10);
    pinMode(9, INPUT);
    pinMode(10, INPUT);

    if (!digitalRead(9)) {
      break;
    }

    upd = !digitalRead(10);

    if (upd) {
      state = !state;
      lcd.clear();
      lcd.print(millis());
    }
    Serial.println(upd);
  }
}

void count() {
  // lcd counts up and down on button pressed
  
  int countState = 0;

  lcd.clear();
  while (true) {
    digitalWrite(9, HIGH);
    digitalWrite(10, HIGH);
    delay(10);
    pinMode(9, INPUT);
    pinMode(10, INPUT);

    if (!digitalRead(9)) {
      break;
    }
    if (digitalRead(10)) {
      pinMode(10, OUTPUT);
      countState--;
      countChanged = true;
    }
    if (countChanged) {
      lcd.print(countState);
    }
    lcd.setCursor(0, 0);
  }
}

void showTemp() {
  // temperature from temperature sensor
  int temperatur = 0;

  // Degree sign.
  // writing custom symbols is described in driver manual.
  byte gradZeichen[8] = {
    B01111,
    B01001,
    B01001,
    B01111,
    B00000,
    B00000,
    B00000,
  };

  //lcd.clear();

  //lcd.createChar(0, smiley);

  lcd.print("Temperatur:");
  lcd.setCursor(15, 1);
  //lcd.write(byte(0)); ...; // cut   // so slooow
  lcd.print("C");

  while (digitalRead(9) == 1) {
    lcd.setCursor(10, 1);
    float res;              // floats
    temperatur = analogRead(analogInPin);
    res = (float) (temperatur - 120 ) / 10;
    Serial.println(res);

    lcd.print(res, 1);

    pinMode(9, OUTPUT);
    digitalWrite(9, HIGH);
    delay(10);
    pinMode(9, INPUT);

    delay(200);
  }
}

void beNice() {
  lcd.clear();
  lcd.print("Hi Reader! Here");
  lcd.setCursor(0, 2);                         // col, row; 0,0
  lcd.print("we go to next.");
}


// text the new prog Name
void showText(char progName[]) {
  for (int i = 0; i < 6; i++) {
    lcd.print(progName);
    delay(300);
    lcd.clear();
    delay(300);
  }
}

void loop() {
  int prog = 0;

  while (true) {
    pinMode(9, OUTPUT);
    digitalWrite(9, HIGH);
    delay(10);
    pinMode(9, INPUT);

    changedProg = !digitalRead(9);

    if (changedProg) {
      prog++;
      switch (prog % 4) {
        case 0:
          showText("*** Be Nice ***");
          beNice();
          break;
        case 1:
          showText("* Temperatur! *");
          showTemp();
          break;
        case 2:
          showText("Press to count");
          count();
          break;
        case 3:
          showText("* StopOnZero *");
          stopGame();
          break;
        default:
          Serial.println("error in loop");
      }
      changedProg = false;
    }
  }
}

